/*
 * Mangle an IP address into a useless format acceptable to GNU libc inet_aton()
 *
 * Accepts an IPv4 IP address as a dotted-quad string.
 * Returns a valid mangled IPv4 IP address as a string.
 *
 * Usage:
 * mangleIP('10.150.4.21');
 * -> "0012.0x96.4.0025"
 */

// Yes, this regex is still ugly.
let valid_ip = new RegExp("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.\
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.\
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.\
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")

const mutators = ['hex', 'HEX', 'oct', 'oct', 'dec', 'dec'];

function randInt(min=2, max=null) {
    if (max === null) {
	max=min;
	min=0;
    }
    return Math.floor(Math.random() * Math.floor(max-min))+min;
}

function choice(items) {
    return items[randInt(items.length)];
}

function strPad(val, padlen=2, padchar="0") {
    return ((Array(padlen+1).join(padchar))+val).slice(0-padlen);
}

function mutate(val, format) {
    let padlen = (val<256) ? 2 : 8;
    switch(format) {
    case 'HEX':
    case 'hex':
	let res = strPad(String(Number(val).toString(16)), padlen);
	return '0x'+((format=="HEX") ? res.toUpperCase() : res);
    case 'oct':
	padlen = (val<256) ? 3 : 12;
	return "0"+strPad(String(Number(val).toString(8)), padlen);
    case 'dec':
	return String(val);
    default:
	throw "Invalid mutator format: "+format;
    }
}

/* This is the lone export. Accepts a string, returns a string. */
export function mangleIP(ip) {
    let valid = valid_ip.exec(ip);
    if (valid===null)
	throw "Invalid IP address: "+ip;
    valid = valid.slice(-4)
    if (choice([true, false, false])) { // 32-bit int
	let res = valid.slice().reverse().reduce(
	    (a, b, i) => (a | Number(b) << (i*8))>>>0);
	return mutate(res, choice(mutators));
    } else { // Dotted-quad
	return valid.map(a => mutate(a, choice(mutators))).join(".");
    }
}
