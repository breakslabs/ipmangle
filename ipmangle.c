#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ipmangle.h"

/* Yes, I know. Pre-processor macros are evil. */
#define randint(a, b) (int)(rand()/(float)(RAND_MAX/(b-a)))+a 

void usage (char *);


int main(int argc, char **argv) {
  char outstr[20];
  
  if (argc != 2) {
    usage(argv[0]);
  }

  if (!mutate_address(argv[1], outstr)) {
    fprintf(stderr, "Invalid IP address: %s\n", argv[1]);
    usage(argv[0]);
  }

  printf("%s\n", outstr);
  
  return(0);
}

/*
 *
 * mutate_address() converts the Internet host address 'address' from the IPv4
 * numbers-and-dots notation to a random notation that is generally less
 * readable, but still able to be parsed by the GNU libc aton() function, and
 * stores it in 'result'. The generated value shouldn't exceed 20 bytes in
 * length, including NULL terminator.
 *
 */
int mutate_address(char *address, char *result) {
  struct in_addr inp;
  unsigned char a, b, c, d;
  char outfmt[25];
  
  if(!inet_aton(address, &inp)) {
    return(0);
  }
  srand(time(NULL));
  a = inp.s_addr & 0xff;
  b = (inp.s_addr & 0xff00)>>8;
  c = (inp.s_addr & 0xff0000)>>16;
  d = (inp.s_addr & 0xff000000)>>24;
  
  if(randint(0, 3)) {
    mutate_fmt(outfmt, 0);
    sprintf(result, outfmt, a, b, c, d);
  } else {
      mutate_fmt(outfmt, 1);
      sprintf(result, outfmt, ntohl(inp.s_addr));
    }
  return(1);
}

void usage(char *progname) {
  fprintf(stderr, "Usage: %s <IPv4 IP address>\n", progname);
  exit(1);
}

/*
 *
 *  mutate_outfmt() generates a randomized output format string for an IP
 *  address, suitable for use with printf. The resultant string is stored in
 *  'fmt', and shouldn't exceed 25 bytes. No terminating newline or other
 *  terminal control is added. If 'bigint' is not 0, the format returned is a
 *  single conversion specification which takes a single 32-bit integer, and
 *  converts it to hex, octal, or decimal. Otherwise, the format string
 *  returned describes the string "a.b.c.d" where a, b, c, and d are each
 *  randomly chosen to convert integer values to hex, octal, or decimal.
 *
 */
void mutate_fmt(char *fmt, unsigned char bigint) {
  int i;
  char *styles[] = {"%04o.", "%#02x.", "%d.", "%#02X.", "%04o.", "%d."};
  char *bigstyles[] = {"%013o", "%#08x", "%u", "%#08X", "%013o", "%u"};
  
  fmt[0] = '\0';
  if(bigint) {
    fmt = strcpy(fmt, bigstyles[randint(0, 6)]);
  } else {
    for (i=0; i<4; i++) {
      strcat(fmt, styles[randint(0, 6)]);
    }
    fmt[strlen(fmt)-1] = '\0';
  }
}
