IPMANGLE
========

A silly utility that mangles a given IP address into one of the
multitude of formats accepted by the GNU libc inet_aton() function.

Python
~~~~~~

No external modules required.

Usage
-----

.. code:: bash

	  $ ipmangle 192.168.5.23
	  0xC0.168.0005.23


Javascript
~~~~~~~~~~

Pretty straight-forward ECMA6 module. No external requirements.
My Javascript isn't stellar, so have at it.

Usage
-----

.. code:: javascript

	  import * as mangler from "./ipmangle.js";
	  console.log(mangler.mangleIP('192.168.43.41'));

C
~~~~~~

Simple C implementation of same. If my C seems a little archaic, it probably
is. The last time I wrote any significant C code, a lot of people were really
up in arms over a blue dress.

Compilation
-----------
The code doesn't really warrant a Makefile, let alone autotools. Just do:

cc -Wall ipmangle.c -o cipmangle

Or copy ipmangle.[ch] into your source tree and have fun.

Usage
-----

Command Line executable
+++++++++++++++++++++

.. code:: bash

	  $ cipmangle 172.24.4.2
	  172.0X18.0004.2

C function call
+++++++++++++++

.. code:: c

	  #include "ipmangle.h"
	  char outstr[20];
	  mutate_address("10.150.77.23", outstr);
	  printf("%s\n", outstr);


License
-------

This code is public domain
